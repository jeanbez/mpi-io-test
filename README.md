# MPI-IO Test Benchmak #

This repository contains a copy of the **MPI-IO Test Benchmark** created by Los Alamos National Lab.

> **Version 1.000.21**
> October 4, 2008

Here you can find the information on the original recovered page:

> Although there are a host of existing file system and I/O test programs available, most are not designed with parallel I/O in mind and are not useful at the scale of the clusters at Los Alamos National Lab (LANL). LANL's MPI-IO Test was written with parallel I/O and scale in mind. The MPI-IO test is built on top of MPI's I/O calls and is used to gather timing information for reading from and writing to file(s) using a variety of I/O profiles; N processes writing to N files, N processes writing to one file, N processes sending data to M processes writing to M files, or N processes sending data to M processes to one file. These diagrams illustrate the various I/O access patterns. A data aggregation capability is available and the user can pass down MPI-IO, ROMIO and file system specific hints. The MPI-IO Test can be used for performance benchmarking and, in some cases, to diagnose problems with file systems or I/O networks.
>
> The newer versions of MPI-IO test have been renamed to fs_test. The most current version of fs_test is currently available on Sourceforge.
> 
> The MPI-IO Test (fs_test) is open sourced under LA-CC-05-013.

This table contains a list of all the versions of the MPI-IO Test benchmark:

> | Release | Date | Source |
> | ------ | ------ | ------ |
> | 1.000.21 | 8 July 2008 | mpi_io_test_21.tgz |
> | 1.000.20 | 13 November 2007 | mpi_io_test_20.tgz |
> | 1.000.09 | 15 December 2006 | mpi_io_test_09.tgz |
> | 1.000.08 | 2 March 2006 | mpi_io_test_08.tgz |

> This code currently lives on Sourceforge.

However, I could not find it on Sourceforge! The page does not exist anymore.

Notice that this repository contains some modifications and improvements on the benchmark!

### Instalation

```sh
$ cd mpi-io-test/test_fs/src
$ make
```